/*
	You can directly pass data into the function by using the parameter
*/

/*
	function functionName(parameter) {
		code block
	}
	functionName(argument);
*/
// name is called parameter
// "parameter" acts as a named variable /container that exists only inside of a function
// It is used to store information that is provided to a function when it is called/invoked

function printName(name) {
	console.log("My name is " + name);
}

// "Juan" and"Miah", the information/data provided directly into the function, it is called an argument
printName("Juana");
printName("Justine");

// Using Multiple Parameters
function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Erven", "Joshua", "Cabral");
// "Erven" will be stored in the parameter "firstName"
// "Joshua" will be stored in the parameter "middleName"
// "Cabral" will be stored in the parameter "lastName"

// In JS, providing more/less arguments thatn the expected paramters will not return an error
createFullName("Eric", "Andales");

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log(`The remainder of ${num} divided by 8 is remainder`);
	let isDivisibleBy8 = (remainder === 0);
	console.log(`Is ${num} divisible by 8?`);
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*
	Mini-Activity:
		1. Create a function which is able to receive data as an argument.
			- This function should be able to receive the name of your favorite superhero
			- Display the name of your favorite superhero in the console.

*/

function displaySuperhero(superhero) {
	console.log(`My favorite superhero is ${superhero}.`);
}

displaySuperhero("Batman");

// Return Statement
/*
	The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
*/

function returnFullName(firstName, middleName, lastName) {
	return firstName + " " + middleName + " " + lastName;
	console.log("Can we print this message?");
}

let completeName = returnFullName("John", "Doe", "Smith");
console.log(completeName);

function returnAddress(city, country) {
	let fullAddress = city + ", " + country;
	return fullAddress
}

let myAddress = returnAddress("Sta. Mesa, Manila", "Philippines");
console.log(myAddress);

/*
	Mini_Activity:
		1. Debug our code. So that the function will be able to return a value and save it in a variable.
*/

function printPlayerInformation(username, level, job) {
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);
	return `Username: ${username}, Level: ${level}, Job: ${job}`;
}

let user1 = printPlayerInformation("cardo123", "999", "Immortal");
console.log(user1);

